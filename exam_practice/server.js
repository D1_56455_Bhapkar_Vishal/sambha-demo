const express = require('express')
const db = require('./db')


const app = express();
app.use(express.json())

app.get("/info", (request, response)=>{
    const connection = db.openConnection();
    const sql = `SELECT * FROM emp`
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data);
    })
})

app.post("/", (request, response)=>{
    const connection = db.openConnection();
    const {id, name, salary, age} = request.body
    const sql = `INSERT INTO emp VALUES(${id}, '${name}', ${salary}, ${age})`
    /* UPDATE emp SET name = "sam", salary = 50000, age=30 WHERE id = 2 */
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})

app.put("/:id", (request, response)=>{
    const connection = db.openConnection();
    const { id } = request.params
    const {name, salary, age} = request.body
    const sql = `UPDATE emp SET name = '${name}', salary = ${salary}, age=${age} WHERE id = ${id}`

    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})

app.delete("/:id", (request, response)=>{
    const connection = db.openConnection();
    const { id } = request.params
   
    const sql = `DELETE FROM emp WHERE id = ${id}`

    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})

app.listen(4000, ()=>{

    console.log("listening at port 4000...!!!")
})

